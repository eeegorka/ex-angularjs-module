'use strict';

app.controller('ModelYearsSearchIndexController', function ($scope, $rootScope, $location, $stateParams, PagerService, ModelYearsService , ModelYearsSearchService, $state, $interval) {
    $rootScope.refresh_loading();
    $scope.model_years = [];

    $scope.page = 'index';
    $rootScope.page_header = "Model Years Search";
    $rootScope.page_description = "View all Model Years";
    $rootScope.search_text = "Model Year Search";
    $rootScope.preloader_text = 'Have you tried the customer export function?';
    $rootScope.event_name = 'view';
    $rootScope.page = {
        name: 'Model Years Search',
        state: 'model_years_search'
    };
    $scope.model_years = [];
    $scope.models = [];
    $scope.makes = [];
    $scope.years = [];

    $rootScope.bread_crumbs = [
        {
            name: 'Home',
            url: $state.href('')
        },
        {
            name: 'Model Years Search',
            url: $state.href('model_years_search')
        }
    ];


    $scope.filter = {
        make_id: ((typeof $stateParams.make_id != "undefined") ? parseInt($stateParams.make_id) : null),
        model_id: ((typeof $stateParams.model_id != "undefined") ? parseInt($stateParams.model_id) : null),
        model_year_id: ((typeof $stateParams.model_year_id != "undefined") ? parseInt($stateParams.model_year_id) : null),
        keyword:((typeof $stateParams.keyword != "undefined") ? parseInt($stateParams.keyword) : null),
    };
    getResults();

    $scope.pager = {};
    $scope.setPage = setPage;
    $scope.getResults = getResults;

    function getResults(){
        ModelYearsSearchService.get({
            model:$scope.filter.model_id,
            make:$scope.filter.make_id,
            year:$scope.filter.model_year_id,
            keyword:$scope.filter.keyword,
        },function (res) {
            if(res.models){
                $scope.models = res.models;
            }
            if(res.makes){
                $scope.makes = res.makes;
            }
            if(res.years){
                for(var year=res.years.min;year<=res.years.max;year++){
                    $scope.years.push(year);
                }
            }
            $scope.model_years = res.model_years.data;
            $rootScope.f_loading($scope.model_years);
            var pages = [];
            for (var i = 1; i <= res.model_years.last_page; i++) {
                pages.push(i);
            }
            $scope.lastpage = pages;
            $scope.last_page = res.model_years.last_page;
            initController();
        });
    }

    function initController() {
        $scope.setPage(1, false);
    }

    function setPage(page, callPage = true) {
        if (page < 1 || $scope.last_page && page > $scope.last_page ) {
            return;
        }
        if (callPage)
            $scope.page(page);
        else
            $scope.pager = PagerService.GetPager($scope.model_years.length, page, 15, $scope.last_page);
    }

    $scope.page = function ($page) {
        $scope.activePage = $page;
        ModelYearsSearchService.get({
            page: $page,
            model:$scope.filter.model_id,
            make:$scope.filter.make_id,
            year:$scope.filter.model_year_id,
            keyword:$scope.filter.keyword,
        }, function (res) {
            $scope.model_years = res.model_years.data;

            var pages = [];
            for (var i = 1; i <= res.model_years.last_page; i++) {
                pages.push(i);
            }
            $scope.lastpage = pages;
            $scope.last_page = res.model_years.last_page;
            $scope.pager = PagerService.GetPager($scope.model_years.length, $page, 20, $scope.last_page);
        });
    };

});