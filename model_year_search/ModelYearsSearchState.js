'use strict';

angular.module('app')
        .config(function ($stateProvider) {
            $stateProvider
                    .state('model_years_search', {
                        parent: 'entity',
                        url: '/model-years-search',
                        data: {
                            pageTitle: 'Model Years Search'
                        },
                        views: {
                            'content@': {
                                templateUrl: '/app/admin/modules/model_year_search/views/index.html',
                                controller: 'ModelYearsSearchIndexController'
                            }
                        }
                    });
        });