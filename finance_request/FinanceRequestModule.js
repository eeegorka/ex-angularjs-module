'use strict';

app.controller('FinanceRequestsIndexController', function ($scope, $rootScope, $location, FinanceRequestService, $state, $interval, PagerService) {
    $rootScope.refresh_loading();
    $scope.finance_requests = [];

    $scope.page = 'index';
    $rootScope.page_header = "Finance Requests";
    $rootScope.page_description = "View all finance_requests in the system";
    $rootScope.search_text = "finance_request";
    $rootScope.create_link = $state.href('finance_requests.edit', {finance_request_id: 0});
    $rootScope.percent = 0;
    $rootScope.event_name = 'view';
    $rootScope.page = {
        name: 'Finance Requests',
        state: 'finance_requests'
    };
    $rootScope.isAddedToFavourite();
    $scope.section_answer = {
        yes: "Yes",
        no: "No",
        no_sure: "Not Sure"
    };
    $scope.settings = {};
    $scope.current_country = false;
    $scope.crop_dates = {};
    $scope.finance_country = null;
    $scope.start_date = null;
    $scope.end_date = null;
    $scope.tooltip_text = 'Attention! If you select all fields, ' +
        'the file  will not contain the total number of results and you will have permissions only for your country data.';
    $scope.all_data_checkbox = false;
    $scope.filter = {
        view_status: "",
    };

    $rootScope.bread_crumbs = [
        {
            name: 'Home',
            url: '#'
        },
        {
            name: 'FinanceRequests',
            url: '#!/finance-requests'
        }
    ];
    var query_string = $location.search();
    if (typeof query_string.status != "undefined") {
        switch (query_string.status) {
            case "updated":
                $rootScope.response.message = "The item has been updated";
                break;
            case "created":
                $rootScope.response.message = "The item has been created";
                break;
        }
    }
    $('#checkbox_tooltip').popover();
    $scope.pager = {};
    $scope.setPage = setPage;
    
    function initController() {
        $scope.setPage(1, false);
    }
    function setPage(page, callPage = true) {
        if (page < 1 || $scope.last_page && page > $scope.last_page ) {
            return;
        }
        if (callPage)
            $scope.page(page);
        else
            $scope.pager = PagerService.GetPager($scope.finance_requests.length, page, 15, $scope.last_page);
    }

    $scope.popoverInfo = 'Select time frame\n' +
        'If you want to get the statistics for all time, leave the dates blank.';


    FinanceRequestService.query({}, function (res) {
        $scope.finance_requests = res.finance_requests.data;
        $scope.settings = res.settings;
        $scope.current_country = res.current_country;
        $rootScope.f_loading($scope.finance_requests);
        var pages = [];
        for (var i = 1; i <= res.finance_requests.last_page; i++) {
            pages.push(i);
        }
        $scope.lastpage = pages;
        $scope.last_page = res.finance_requests.last_page;
        initController();
        $('#PopoverExport').popover();
    });
    
    $scope.page = function ($page) {
        $scope.activePage = $page;
        FinanceRequestService.query({page: $page, view_status: $scope.filter.view_status, search: $scope.searchText}, function (res) {
            $scope.finance_requests = res.finance_requests.data;

            var pages = [];
            for (var i = 1; i <= res.finance_requests.last_page; i++) {
                pages.push(i);
            }
            $scope.lastpage = pages;
            $scope.last_page = res.finance_requests.last_page;
            $scope.pager = PagerService.GetPager($scope.finance_requests.length, $page, 20, $scope.last_page);
        });
    };
    
    $scope.searchUsers = function(){
        $scope.setPage(1, true);
    };

    var x,left,down;
    $('.scroll-table').bind({
        mousedown: function (e) {
            e.preventDefault();
            down = true;
            x = e.pageX;
            left =  $(this).scrollLeft();
        },
        mousemove: function(e) {
            e.preventDefault();
            if(down){
                var newX = e.pageX;
                $(this).scrollLeft(left - newX + x);
            }
        },
        mouseup: function (e) {
            e.preventDefault();
            down = false;
        }
    });

    $('body').mouseup(function(e){
        down = false;
    });

    $('.scroll-table').on('click','.table-long-field',function (e){
        var element = e.currentTarget;
        if($(element).hasClass('active')){
            $(element).removeClass('active');
        }else{
            $('.scroll-table').find('.active').each(function (key,section) {
                $(section).removeClass('active');
            });
            $(element).addClass('active');
        }
    });
});


app.controller('FinanceRequestsEditController', function ($scope, $rootScope, $state, $stateParams, $location, FinanceRequestService, MakesService, Upload, $timeout) {
    $scope.finance_request = {
        id: $stateParams.finance_request_id
    };

    $rootScope.searchText = '';
    $scope.page = 'edit';
    $rootScope.page_header = "View Finance Request";
    $rootScope.page_description = "Create/Edit a Finance Request";
    $rootScope.search_text = "finance_request";
    $rootScope.create_link = '';
    $rootScope.event_name = (($scope.finance_request.id) != 0 ? "update" : "create");

    $scope.section_answer = {
        yes: "Yes",
        no: "No",
        no_sure: "Not Sure"
    };

    $scope.urgency = {
        '2-4': "2 weeks - 4 weeks",
        '4-6': "4 weeks - 6 weeks",
        no_sure: "Not Sure",
        'Not Urgent': "Not Urgent",
        'Normal': "Normal",
        'Urgent': "Urgent",
        "Very Urgent": "Very Urgent",
    };

    $rootScope.bread_crumbs = [
        {
            name: 'Home',
            url: '#'
        },
        {
            name: 'FinanceRequests',
            url: $state.href('finance-requests')
        }
    ];

    FinanceRequestService.change_view_status({id: $scope.finance_request.id});

    FinanceRequestService.get({id: $scope.finance_request.id}, function (res) {
        console.log("res", res);
        $scope.finance_request = res;
    });

});

app.controller('FinanceRequestsAdditionalIndexController', function ($scope, $rootScope, $location, FinanceRequestService, $state, $interval, PagerService) {
    $rootScope.refresh_loading();
    $scope.finance_requests_additional = [];

    $scope.page = 'index';
    $rootScope.page_header = "Finance Requests Additional";
    $rootScope.page_description = "View additional finance_requests in the system";
    $rootScope.search_text = "finance_request_additional";
    $rootScope.create_link = $state.href('finance_request.edit', {finance_request_additional_id: 0});
    $rootScope.percent = 0;
    $rootScope.event_name = 'view';
    $rootScope.page = {
        name: 'Finance Requests Additional',
        state: 'finance_request_additional'
    };
    $rootScope.isAddedToFavourite();

    $scope.filter = {
        view_status: "",
    };

    $rootScope.bread_crumbs = [
        {
            name: 'Home',
            url: '#'
        },
        {
            name: 'FinanceRequestsAdditional',
            url: '#!/finance-requests-additional'
        }
    ];

    var query_string = $location.search();
    if (typeof query_string.status != "undefined") {
        switch (query_string.status) {
            case "updated":
                $rootScope.response.message = "The item has been updated";
                break;
            case "created":
                $rootScope.response.message = "The item has been created";
                break;
        }
    }
    $scope.pager = {};
    $scope.setPage = setPage;

    function initController() {
        $scope.setPage(1, false);
    }
    function setPage(page, callPage = true) {
        if (page < 1 || $scope.last_page && page > $scope.last_page ) {
            return;
        }
        if (callPage)
            $scope.page(page);
        else
            $scope.pager = PagerService.GetPager($scope.finance_requests_additional.length, page, 15, $scope.last_page);
    }

    FinanceRequestService.query_additional({}, function (res) {
        $scope.finance_requests_additional = res.finance_requests_additional.data;
        $rootScope.f_loading($scope.finance_requests_additional);
        var pages = [];
        for (var i = 1; i <= res.finance_requests_additional.last_page; i++) {
            pages.push(i);
        }
        $scope.lastpage = pages;
        $scope.last_page = res.finance_requests_additional.last_page;
        initController();
    });

    $scope.page = function ($page) {
        $scope.activePage = $page;
        FinanceRequestService.query_additional({page: $page, view_status: $scope.filter.view_status, search: $scope.searchText}, function (res) {
            $scope.finance_requests_additional = res.finance_requests_additional.data;

            var pages = [];
            for (var i = 1; i <= res.finance_requests_additional.last_page; i++) {
                pages.push(i);
            }
            $scope.lastpage = pages;
            $scope.last_page = res.finance_requests_additional.last_page;
            $scope.pager = PagerService.GetPager($scope.finance_requests_additional.length, $page, 20, $scope.last_page);
        });
    };

    $scope.searchUsers = function(){
        $scope.setPage(1, true);
    };

    var x,left,down;
    $('.scroll-table').bind({
        mousedown: function (e) {
            e.preventDefault();
            down = true;
            x = e.pageX;
            left =  $(this).scrollLeft();
        },
        mousemove: function(e) {
            e.preventDefault();
            if(down){
                var newX = e.pageX;
                $(this).scrollLeft(left - newX + x);
            }
        },
        mouseup: function (e) {
            e.preventDefault();
            down = false;
        }
    });

    $('body').mouseup(function(e){
        down = false;
    });

    $('.scroll-table').on('click','.table-long-field',function (e){
        var element = e.currentTarget;
        if($(element).hasClass('active')){
            $(element).removeClass('active');
        }else{
            $('.scroll-table').find('.active').each(function (key,section) {
                $(section).removeClass('active');
            });
            $(element).addClass('active');
        }
    });
});