'use strict';

angular.module('app')
    .config(function ($stateProvider) {
        $stateProvider
            .state('finance-requests', {
                parent: 'entity',
                url: '/finance-requests',
                data: {
                    pageTitle: 'FinanceRequests'
                },
                views: {
                    'content@': {
                        templateUrl: '/app/admin/modules/finance_request/views/index.html',
                        controller: 'FinanceRequestsIndexController'
                    }
                }
            })
            .state('finance-requests.edit', {
                parent: 'finance-requests',
                url: '/{finance_request_id}/edit',
                data: {
                    hide_basic_form_page: false
                },
                views: {
                    'content@': {
                        templateUrl: '/app/admin/modules/finance_request/views/edit.html',
                        controller: 'FinanceRequestsEditController'
                    }
                },
                resolve: {

                }
            })
            .state('finance-requests-additional', {
                parent: 'entity',
                url: '/finance-requests-additional',
                data: {
                    pageTitle: 'FinanceRequestsAdditional'
                },
                views: {
                    'content@': {
                        templateUrl: '/app/admin/modules/finance_request/views/index_additional.html',
                        controller: 'FinanceRequestsAdditionalIndexController'
                    }
                }
            });
    });