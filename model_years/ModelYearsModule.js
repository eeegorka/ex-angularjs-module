'use strict';

app.controller('ModelYearsIndexController', function ($scope, $rootScope, $location, $stateParams, ModelYearsService, Make, Model, $state, $interval) {
    $rootScope.refresh_loading();
    $scope.model_years = [];

    $scope.make = Make;
    $scope.model = Model;

    $scope.page = 'index';
    $rootScope.page_header = "Model Years";
    $rootScope.page_description = "View all Model Years";
    $rootScope.search_text = "Model Year";
    $rootScope.create_link = $state.href('makes.edit.models.edit.model_years.edit', {make_id: $stateParams.make_id, model_year_id: 0});
    $rootScope.preloader_text = 'Have you tried the customer export function?';
    $rootScope.percent = 0;
    $rootScope.event_name = 'view';
    $rootScope.page = {
        name: 'Model Years',
        state: 'makes.edit.models.edit.model_years'
    };
    $rootScope.isAddedToFavourite();
    $rootScope.bread_crumbs = [
        {
            name: 'Home',
            url: $state.href('dashboard')
        },
        {
            name: 'Model Years',
            url: $state.href('makes.edit.models.edit.model_years')
        }
    ];

    var percent = 0;
    $rootScope.promise_interval = $interval(function () {
        if (percent == 100) {
            $interval.cancel($rootScope.promise_interval);
        }
        $rootScope.percent = percent + '%';
        percent++;
    }, 100);

    var query_string = $location.search();
    if (typeof query_string.status != "undefined") {
        switch (query_string.status) {
            case "updated":
                $rootScope.response.message = "The item has been updated";
                break;
            case "created":
                $rootScope.response.message = "The item has been created";
                break;
        }
    }

    ModelYearsService.query({make_id: $stateParams.make_id, model_id: $stateParams.model_id}, function (res) {
        $scope.model_years = res.model_years;
        $rootScope.f_loading($scope.model_years);
    });


    $scope.switchFormRequestDeal = function (id, e)
    {
        if ($(e).hasClass('active')) {
            $(e).removeClass('active').addClass('disabled');
        }
        ModelYearsService.switchForm({model_year_id: id});
    };

    $scope.changeModelYearStatus = function (model_year) {
        ModelYearsService.changeStatus({id:model_year.id}, function (res) {
            if(res.status){
                model_year.status = !model_year.status;
            }
        });
    };

});


app.controller('ModelYearsEditController', function ($scope, $rootScope, $state, $stateParams, $location, ModelYearsService, UsersService, $timeout, Make, Model, Upload, BaseColours,LifestylesService) {
    $scope.model_year = {
        id: $stateParams.model_year_id,
        make_id: $stateParams.make_id,
        model_id: $stateParams.model_id,
        slug: '',
        name: ''
    };
    $rootScope.search_text = "";
    $scope.make = Make;
    $scope.model = Model;
    $scope.images = [];

    $scope.base_colours = BaseColours;

    $scope.options = {
        placeholder: 'rgba(1,2,3,0.0)',
        format: 'hex',
        swatch: true,
        swatchPos: 'left',
        swatchBootstrap: true
    };

    $scope.page = 'edit';
    $rootScope.event_name = (($scope.model_year.id) != 0 ? "update" : "create");

    $rootScope.isAddedToFavourite();
    $rootScope.bread_crumbs = [
        {
            name: 'Home',
            url: $state.href('dashboard')
        },
        {
            name: 'Model Years',
            url: $state.href('makes.edit.models.edit.model_years')
        }
    ];

    Make.$promise.then(function (data) {
        $rootScope.page_header = "Create/Edit Model Year in " + data.name;
        $rootScope.page_description = "Create/Edit a model year";
    });

    $rootScope.search_text = "model";
    $rootScope.create_link = '';
    $scope.$watch('searchText', function (newValue, oldValue, scope) {
        if (typeof newValue != "undefined" && newValue.length > 0) {
             $location.path("/models");
        }
    });
    
    ModelYearsService.get({
        make_id: $stateParams.make_id,
        model_id: $stateParams.model_id,
        id: $stateParams.model_year_id
    }, function (res) {
        $scope.model_year = res;


        if (typeof $scope.model_year.model_year_images == "undefined") {
            $scope.model_year.model_year_images = new Array();
        }
        if (typeof $scope.model_year.model_year_colours == "undefined") {
            $scope.model_year.model_year_colours = new Array();
        }
        if (typeof $scope.model_year.model_year_videos == "undefined") {
            $scope.model_year.model_year_videos = new Array();
        }

        if (typeof $scope.model_year.cons == null) {
            $scope.model_year = {
                cons: []
            };
        }

        if (typeof $scope.model_year.pros == null) {
            $scope.model_year = {
                pros: []
            };
        }

        if (typeof $scope.model_year.model_year_expert_review == "undefined") {
            $scope.model_year.model_year_expert_review = {
                published_at: null,
                pros: [],
                cons: []
            };
        } else {
            if ($scope.model_year.model_year_expert_review.published_at != "" && $scope.model_year.model_year_expert_review.published_at != null) {
                $scope.model_year.model_year_expert_review.published_at = new Date($scope.model_year.model_year_expert_review.published_at);
            }
        }
        $scope.model_year.banner_image = null;
        $scope.model_year.banner_mobile_image = null;
        $scope.model_year.search_image = null;
    });


    var onSaveSuccess = function (result) {
//        $scope.$emit('webshopApp:shopCatalogUpdate', result);
        if($location.$$search.search_page === true){
            $state.go("model_years_search");
        }else{
            $state.go("makes.edit.models.edit.model_years", {
                make_id: $stateParams.make_id,
                model_id: $stateParams.model_id
            });
        }
        $scope.isSaving = false;
    };

    var onSaveError = function (result) {
        $scope.isSaving = false;
    };

    $scope.save = function () {
        $scope.isSaving = true;
        if ($scope.model_year.id != 0 && $scope.model_year.id != null) {

            ModelYearsService.update({
                make_id: $stateParams.make_id,
                model_id: $stateParams.model_id,
                id: $stateParams.model_year_id
            }, $scope.model_year, onSaveSuccess, onSaveError);
        } else {
            $scope.model_year.make_id = $stateParams.make_id;
            $scope.model_year.model_id = $stateParams.model_id;
            $scope.model_year.id = $stateParams.model_year_id;
            ModelYearsService.save({
                make_id: $scope.model_year.make_id,
                model_id: $scope.model_year.model_id
            }, $scope.model_year, onSaveSuccess, onSaveError);
        }
    };

    $scope.uploadFiles = function (files) {
        let promise = $timeout();
        $scope.files = files;
//        $scope.model_year.model_year_images = [];
        angular.forEach(files, function (file, key) {
            promise = promise.then(function() {
                if (file && !file.$error) {
                    file.upload = Upload.upload({
                        url: $rootScope.admin_api_url + "makes/" + $stateParams.make_id + "/models/" + $stateParams.model_id + "/model_years/" + $stateParams.model_year_id + "/upload-image?" + Math.random(),
                        file: file
                    });

                    file.upload.then(function (response) {
                            $timeout(function () {
                               file.result = response.data;
                               $scope.images.push(file.result.new_image);

                                $scope.filterByNames($scope.images);

                               console.log($scope.images);
                            });
                    }, function (response) {
                        if (response.status > 0) {
                            $scope.errorMsg = response.status + ': ' + response.data;
                        }
                    });

                    file.upload.progress(function (evt) {
                        file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
                    });
                }
                console.log("uploading file" + file.name);
                return $timeout(50);
            });
            
        });

    };

    $scope.filterByNames = function (data) {
        data.sort(function(a, b) {
            return (a.name > b.name) ? 1 : ((b.name > a.name) ? -1 : 0);
        });
        var remove_indexes = new Array();
        var j = 0;
        for(var i in $scope.model_year.model_year_images) {
            
            console.log("before removing all new", $scope.model_year.model_year_images[i].name, $scope.model_year.model_year_images[i].status, $scope.model_year.model_year_images[i].tmp_image_name_original);
            if($scope.model_year.model_year_images[i].status == "temp") {
                remove_indexes[j++] = i;
            }
        }
        for(var k = remove_indexes.length-1; k >= 0; k--){
            console.log("splicing", $scope.model_year.model_year_images[remove_indexes[k]].name);
            $scope.model_year.model_year_images.splice(remove_indexes[k], 1);
        }
        
        for(var i in data) {
            $scope.model_year.model_year_images.push(data[i]);
        }
        
    };

    $scope.uploadImage = function (files, type) {
        type = (type == null || typeof type === 'undefined') ? 'search' : type;
        $scope.files = files;
        angular.forEach(files, function (file) {
            if (file && !file.$error) {
                file.upload = Upload.upload({
                    url: $rootScope.admin_api_url + "makes/" + $stateParams.make_id + "/models/" + $stateParams.model_id + "/model_years/" + $stateParams.model_year_id + "/upload-image?" + Math.random(),
                    file: file
                });

                file.upload.then(function (response) {
                    $timeout(function () {
                        file.result = response.data;
                        if (type == 'search') {
                            $scope.model_year.search_image = response.data.new_image;
                            $scope.model_year.search_image_thumb_url = response.data.new_image.url_thumb;
                        }
                        if(type == "banner"){
                            $scope.model_year.banner_image = response.data.new_image;
                            $scope.model_year.banner_image_thumb_url = response.data.new_image.url_thumb;
                        }
                        if(type == "banner_mobile"){
                            $scope.model_year.banner_mobile_image = response.data.new_image;
                            $scope.model_year.banner_mobile_image_thumb_url = response.data.new_image.url_thumb;
                        }
                    });
                }, function (response) {
                    if (response.status > 0) {
                        $scope.errorMsg = response.status + ': ' + response.data;
                    }
                });

                file.upload.progress(function (evt) {
                    file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
                });
            }
        });
    };

    $scope.makeFrontpageImage = function (idx) {
        for (var i in $scope.model_year.model_year_images) {
            if (idx == i) {
                $scope.model_year.model_year_images[i].is_frontpage = 1;
            } else {
                $scope.model_year.model_year_images[i].is_frontpage = 0;
            }
        }
    };

    $scope.removeImage = function (idx) {
        if(typeof $scope.model_year.model_year_images[idx].status != "undefined" && $scope.model_year.model_year_images[idx].status == "temp") {
            for(var i = $scope.images.length -1; i >=0; i--) {
                if($scope.model_year.model_year_images[idx].tmp_image_name_original == $scope.images[i].tmp_image_name_original) {
                    $scope.images.splice(i,1);
                    break;
                }
            }
        }
        $scope.model_year.model_year_images.splice(idx, 1);
    };


    $scope.addVideo = function () {
        $scope.model_year.model_year_videos.push({
            name: "",
            video_url: "",
            provider: "youtube",
            description: ""

        });
    };

    $scope.removeVideo = function (idx) {
        $scope.model_year.model_year_videos.splice(idx, 1);
    };


    $scope.uploadColourFiles = function (files) {
        $scope.files = files;
        angular.forEach(files, function (file) {
            if (file && !file.$error) {
                file.upload = Upload.upload({
                    url: $rootScope.admin_api_url + "makes/" + $stateParams.make_id + "/models/" + $stateParams.model_id + "/model_years/" + $stateParams.model_year_id + "/upload-colour-image?" + Math.random(),
                    file: file
                });

                file.upload.then(function (response) {
                    $timeout(function () {
                        file.result = response.data;
                        $scope.model_year.model_year_colours.push(file.result.new_image);
                    });
                }, function (response) {
                    if (response.status > 0) {
                        $scope.errorMsg = response.status + ': ' + response.data;
                    }
                });

                file.upload.progress(function (evt) {
                    file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
                });
            }
        });
    };

    $scope.removeColour = function (idx) {
        $scope.model_year.model_year_colours.splice(idx, 1);
    };

    $scope.popup1 = {
        opened: false
    };

    $scope.open1 = function () {
        $scope.popup1.opened = true;
    };

    $scope.refreshUsers = function (keyword) {
        var params = {keyword: keyword, sensor: false};
        return UsersService.query({params: params}, function (response) {
            $scope.users = response.users.data;
        });
    };
    $scope.addPros = function () {
        if ($scope.model_year.pros == null) {
            $scope.model_year.pros = new Array();
        }

        $scope.model_year.pros.push({value: ""});
    };
    $scope.removePros = function (idx) {
        $scope.model_year.pros.splice(idx, 1);
    };
    $scope.addCons = function () {
        if ($scope.model_year.cons == null) {
            $scope.model_year.cons = new Array();
        }
        $scope.model_year.cons.push({value: ""});
    };
    $scope.removeCons = function (idx) {
        $scope.model_year.cons.splice(idx, 1);
    };
    
     $scope.refreshSimilar = function (keyword) {
        var params = {keyword: keyword, sensor: false, make_id: 0, model_id: 0};
        return ModelYearsService.query(params, function (response) {
            $scope.similar = response.model_years.data;
        });
    };
    $scope.refreshLifestyles = function (keyword) {
        var params = {search: keyword};
        return LifestylesService.getLifestyleByName(params, function (response) {
            $scope.lifestyles = response.lifestyles;
        });
    };
});

app.controller('ModelYearsDeleteController', function ($scope, $rootScope, $stateParams, $location, ModelYearsService, $timeout, $state) {
    $scope.model_year = {
        id: $stateParams.model_year_id,
        name: '',
        display_name: ''
    };
    $rootScope.search_text = "";
    $scope.page = 'delete';
    $rootScope.page_header = "Delete Model Year";
    $rootScope.page_description = "Delete the model year";
    $rootScope.event_name = 'delete';

    $rootScope.isAddedToFavourite();
    $rootScope.bread_crumbs = [
        {
            name: 'Home',
            url: $state.href('dashboard')
        },
        {
            name: 'Model Years',
            url: $state.href('makes.edit.models.edit.model_years')
        }
    ];

   $scope.$watch('searchText', function (newValue, oldValue, scope) {
        if (typeof newValue != "undefined" && newValue.length > 0) {
             $location.path("/models");
        }
    });
    ModelYearsService.get({
        make_id: $stateParams.make_id,
        model_id: $stateParams.model_id,
        id: $stateParams.model_year_id
    }, function (res) {
        $scope.model_year = res;
    });

    $scope.cancel = function () {
        $state.go("makes.edit.models.edit.model_years", {
            make_id: $stateParams.make_id,
            model_id: $stateParams.model_id
        });
    };

    $scope.delete = function () {
        $scope.isSaving = true;
        ModelYearsService.delete({
            make_id: $stateParams.make_id,
            model_id: $stateParams.model_id,
            id: $stateParams.model_year_id
        }, onDeleteSuccess, onDeleteError);
    };

    var onDeleteSuccess = function (result) {
        $state.go("makes.edit.models.edit.model_years", {
            make_id: $stateParams.make_id,
            model_id: $stateParams.model_id
        });
        $scope.isSaving = false;
    };

    var onDeleteError = function (result) {
        $scope.isSaving = false;
    };

});