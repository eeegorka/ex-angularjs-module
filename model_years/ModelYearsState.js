'use strict';

angular.module('app')
        .config(function ($stateProvider) {
            $stateProvider
                    .state('makes.edit.models.edit.model_years', {
                        parent: 'makes.edit.models.edit',
                        url: '/model_years',
                        data: {
                            pageTitle: 'Model Years'
                        },
                        views: {
                            'content@': {
                                templateUrl: '/app/admin/modules/model_years/views/index.html',
                                controller: 'ModelYearsIndexController'
                            }
                        },
                        resolve: {
                            Model: ['ModelsService', '$stateParams', function(ModelsService, $stateParams) {
                                return ModelsService.get({make_id: $stateParams.make_id ,id : $stateParams.model_id});
                            }]
                        }
                    })
                    .state('makes.edit.models.edit.model_years.edit', {
                        parent: 'makes.edit.models.edit.model_years',
                        url: '/{model_year_id}/edit',
                        data: {
                            hide_basic_form_page: false
                        },
                        views: {
                            'content@': {
                                templateUrl: '/app/admin/modules/model_years/views/edit.html',
                                controller: 'ModelYearsEditController'
                            }
                        },
                        resolve: {
                            BaseColours: ['$resource', '$stateParams', function($resource, $stateParams) {
                                return $resource("/admin/base_colours").query();
                            }]
                        }
                    });
        });